#ifndef __VARIANT_HPP__
#define __VARIANT_HPP__
#include <stdio.h>
#include <variant>
#include <memory>
#include <vector>
#include <unordered_map>
#include <map>
#include <cstddef>
#include <iostream>
#include <stdint.h>
#include <string>
#include <sstream>
#include <typeinfo>
#include <typeindex>
#include <iterator>
#include <initializer_list>
#include <iostream>
#include <fstream>

#include <rapidjson/istreamwrapper.h>
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/reader.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include <MemoryBuffer.hpp>

//#include <absl/container/flat_hash_map.h>
//#include <absl/container/inlined_vector.h>
//#include <msgpack.hpp>

#include <stack>


// deep pointer class from https://codereview.stackexchange.com/questions/103744/deepptra-deep-copying-unique-ptr-wrapper-in-c/103792


namespace variant_impl {
  template <unsigned N> class variant;
  
 template<typename T>
    class DeepPtr {
    public:
   DeepPtr() noexcept : myPtr(std::make_unique<T>() ) { }
   DeepPtr(const T &value)  noexcept: myPtr( std::make_unique<T>(value)) {};

   DeepPtr(const DeepPtr &other) noexcept : myPtr(nullptr) {
     //     std::cout << "DeepPtr(const DeepPtr &other)" <<std::endl;
     if (other) {
       myPtr = std::make_unique<T>(*other); 
     }
   }
   
   DeepPtr(DeepPtr &&other) noexcept: myPtr(nullptr)  {
     //     std::cout << "DeepPtr(DeepPtr &&other)" <<std::endl;
     if (other) {        
       myPtr=std::move(other.myPtr);
     }
   }
   
   DeepPtr &operator=(const DeepPtr &other) noexcept {
     //     std::cout << "DeepPtr &operator=(const DeepPtr &other)"<< std::endl;
     DeepPtr temp(other);
     std::swap(*this, temp);
     return *this;
   }
   
   DeepPtr &operator=(DeepPtr &&other) noexcept  {
     //     std::cout << "DeepPtr &operator=(DeepPtr &&other)"<< std::endl;
     myPtr=std::move(other.myPtr);
     return *this;
   }
   
   //        static void swap(DeepPtr &left, DeepPtr &right) { std::swap(left.myPtr, right.myPtr); }
   
   T &operator*() { return *myPtr; }
   
   const T &operator*() const { return *myPtr; }
   
   // T* const operator->() { return myPtr.operator->(); }
   T *const operator->() { return myPtr.operator->(); }
   
   T *operator->() const { return myPtr.operator->(); }
   
   T *get() const { return myPtr.get(); }
   
   operator bool() const { return (bool) myPtr; }

 private:
   std::unique_ptr<T> myPtr;
 };

  
    template<typename Test, template<typename...> class Ref>
    struct is_specialization : std::false_type {
    };

    template<template<typename...> class Ref, typename... Args>
    struct is_specialization<Ref<Args...>, Ref> : std::true_type {
    };
    template<class T>
    struct constructs_string : std::integral_constant<bool, false> {
    };
    template<>
    struct constructs_string<char> : std::integral_constant<bool, true> {
    };
    template<>
    struct constructs_string<char *> : std::integral_constant<bool, true> {
    };
    template<>
    struct constructs_string<const char *> : std::integral_constant<bool, true> {
    };
    template<>
    struct constructs_string<std::string> : std::integral_constant<bool, true> {
    };
    template<>
    struct constructs_string<std::initializer_list<char>> : std::integral_constant<bool, true> {
    };
  template<typename K, unsigned N,typename T=std::unordered_map<K,variant<N>>>
  class MapPtr : public DeepPtr<T> {
    using DeepPtr<T>::DeepPtr;
  };
  
  template<unsigned N=64>
    class variant {
    public:
       using array_t=std::vector<variant>;
    //using array_t=absl::InlinedVector<variant,16>;
        using string_t=std::string;
        using size_type=std::size_t;
    using object_t=std::map<const std::string,variant>;
  //using object_t=absl::flat_hash_map<std::string,variant>; 
    //        class ArrayPtr : public DeepPtr<array_t> {
    //        using DeepPtr<array_t>::DeepPtr;
    //    }
    using ArrayPtr=DeepPtr<array_t>;
    using ObjPtr= DeepPtr<object_t>;
    using StrPtr= DeepPtr<string_t>;
    using VariantPtr=std::unique_ptr<variant>;
    template<typename T, typename A=std::vector<T>>
      class ValPtr : public DeepPtr<A> {
	using DeepPtr<A>::DeepPtr;
      };


    template<typename... T> using variant_t=std::variant<T...>;
#define get_v std::get
#define visit_v std::visit
struct dummy_double {
  float v;
};
struct dummy_uint64 {
  uint32_t v;
};
struct dummy_int64 {
  int32_t v;
};
using double_type=typename std::conditional<N==64, double, dummy_double>::type;
using uint64_type=typename std::conditional<N==64, uint64_t, dummy_uint64>::type;
using int64_type=typename std::conditional<N==64, int64_t, dummy_int64>::type;
using DBL=typename std::conditional<N==64, double, float>::type;
using U64=typename std::conditional<N==64, uint64_t, uint32_t>::type;
using I64=typename std::conditional<N==64, int64_t, int32_t>::type;

        using value_t =variant_t<std::nullptr_t,
                uint64_type,
                uint32_t,
                uint16_t,
                uint8_t,
                int64_type,
                int32_t,
                int16_t,
                int8_t,
                float,
                double_type,
                bool,
                ArrayPtr,
                ObjPtr,
                StrPtr,
                ValPtr<uint64_type>,
                ValPtr<int64_type>,
                ValPtr<uint32_t>,
                ValPtr<int32_t>,
                ValPtr<uint16_t>,
                ValPtr<int16_t>,
                ValPtr<uint8_t>,
                ValPtr<int8_t>,
                ValPtr<float>,
                ValPtr<double_type>,
                ValPtr<bool>
        >;

        using value_type = variant;
        using reference = value_type &;
        using pointer = const value_type *;
        using const_reference = const value_type &;
    public:
        enum class Type {
            null,
            uint64,
            uint32,
            uint16,
            uint8,
            int64,
            int32,
            int16,
            int8,
            float32,
            float64,
            boolean,
            array,
            object,
            string,
            array_uint64,
            array_int64,
            array_uint32,
            array_int32,
            array_uint16,
            array_int16,
            array_uint8,
            array_int8,
            array_float,
            array_double,
            array_bool
        };

        variant() : value(nullptr) {}

        variant(const variant &other) {
            value = other.value;
        }
        variant(variant &&other)    {
	  value=std::move(other.value);
        }
        variant(ObjPtr &&other) {
	  value=ObjPtr();
	  get_v<ObjPtr>(value)=std::move(other);
        }
        variant(StrPtr &&other) {
	  value=StrPtr();
	  get_v<StrPtr>(value)=std::move(other);
        }
         variant(ArrayPtr &&other) {
	  value=ArrayPtr();
	  get_v<ArrayPtr>(value)=std::move(other);
        }
        template <typename T> 
	variant(ValPtr<T> &&other) {
	  value=ValPtr<T>();
	  get_v<ValPtr<T>>(value)=std::move(other);
        }
        

        variant(Type t) {
            switch (t) {
                case Type::null:
                    value = nullptr;
                    break;
                case Type::uint64:
                    value = U64(0);
                    break;
                case Type::int64:
                    value = I64(0);
                    break;
                case Type::uint32:
                    value = uint32_t(0);
                    break;
                case Type::int32:
                    value = int32_t(0);
                    break;
                case Type::uint16:
                    value = uint16_t(0);
                    break;
                case Type::int16:
                    value = int16_t(0);
                    break;
                case Type::uint8:
                    value = uint8_t(0);
                    break;
                case Type::int8:
                    value = int8_t(0);
                    break;
                case Type::float32:
                    value = float(0.);
                    break;
                case Type::float64:
                    value = DBL(0.);
                    break;
                case Type::boolean:
                    value = bool(false);
                    break;
                case Type::array:
                    value = ArrayPtr();
                    break;
                case Type::object:
                    value = ObjPtr();
                    break;
                case Type::string:
                    value = StrPtr();
                    break;
                case Type::array_uint64:
                    value = ValPtr<U64>();
                    break;
                case Type::array_int64:
                    value = ValPtr<I64>();
                    break;
                case Type::array_uint32:
                    value = ValPtr<uint32_t>();
                    break;
                case Type::array_int32:
                    value = ValPtr<int32_t>();
                    break;
                case Type::array_uint16:
                    value = ValPtr<uint16_t>();
                    break;
                case Type::array_int16:
                    value = ValPtr<int16_t>();
                    break;
                case Type::array_uint8:
                    value = ValPtr<uint8_t>();
                    break;
                case Type::array_int8:
                    value = ValPtr<int8_t>();
                    break;
                case Type::array_float:
                    value = ValPtr<float>();
                    break;
                case Type::array_double:
                    value = ValPtr<DBL>();
                    break;
                case Type::array_bool:
                    value = ValPtr<bool>();
                    break;
            }
        }

    //        variant &operator=(variant other) {
    //         std::swap(value, other.value);
    //        return *this;
    //    }
    variant &operator=(const variant &other) = default;

        static variant array() {
            return variant(Type::array);
        }

        static variant object() {
            return variant(Type::object);
        }

        template<typename T>
        variant(T arg)  {
	  if constexpr(std::is_same_v<T,double>) 
	      value=DBL(arg);
	  else if  constexpr(std::is_same_v<T,uint64_t>)
              value=U64(arg);
	  else if  constexpr(std::is_same_v<T,int64_t>)
	      value=I64(arg);
          else value=arg;
	}

        template<typename T>
        variant(std::vector<T> arg) {
            value = ValPtr<T>(std::vector<T>(arg));
        }

	//      variant(const char *arg) {
	//      value = StrPtr(std::string(arg));
	// }
	variant(const char *arg) {	
	  //	  std::cout << "arg " << std::string(arg) << std::endl; 
	  value=StrPtr(std::string(arg));
	}

        variant(std::initializer_list<variant> init,
                bool type_deduction = true,
                Type manual_type = Type::array) {
            bool is_an_object = true;
            for (const auto &element : init) {
                if (not element.is_array() or element.size() != 2
                    or not element[0].is_string()) {
                    is_an_object = false;
                    break;
                }
            }
            if (not type_deduction) {
                if (manual_type == Type::array) {
                    is_an_object = false;
                }
                if (manual_type == Type::object and not is_an_object) {
                    throw;
                }
            }
            if (is_an_object) {
                value = ObjPtr();
                for (auto &element : init) {
                    get_v<ObjPtr>(value)->emplace(
                            std::move(*(get_v<StrPtr>(element[0].value))),
                            std::move(element[1]));
                }
            } else {
                value = ArrayPtr(std::move(init));
            }
        }

        U64 getUint() const {
            if constexpr(N==64)
              if (index() == Type::uint64) return get_v<U64>(value);
            if (index() == Type::uint32) return get_v<uint32_t>(value);
            if (index() == Type::uint16) return get_v<uint16_t>(value);
            if (index() == Type::uint8) return get_v<uint8_t>(value);
	    if (is_bool()) return U64(get_v<bool>(value));
            throw;
        }

        I64 getInt() const {
	  //	  std::cout << "getInt " << type_name() << std::endl;
            if constexpr(N==64)
              if (index() == Type::int64) return get_v<I64>(value);
            if (index() == Type::int32) return get_v<int32_t>(value);
            if (index() == Type::int16) return get_v<int16_t>(value);
            if (index() == Type::int8) return get_v<int8_t>(value);	    
	    if (is_bool()) return I64(get_v<bool>(value));
            throw;
        }

        DBL getFloat() const {
            if constexpr(N==64)
               if (index() == Type::float64) return get_v<DBL>(value);
            if (index() == Type::float32) return get_v<float>(value);
	    if (is_unsigned()) return getUint();
	    if (is_signed()) return getInt();
	    if (is_bool()) return DBL(get_v<bool>(value));
            throw;
        }


        bool is_float() const { return index() == Type::float64 || index() == Type::float32; }

        bool is_integer() const { return index() >= Type::uint64 && index() <= Type::int8; }

        bool is_number() const { return is_float() || is_integer(); }

        bool is_signed() const { return index() >= Type::int64 && index() <= Type::int8; }

        bool is_unsigned() const { return index() >= Type::uint64 && index() <= Type::uint8; }

        bool is_bool() const { return index() == Type::boolean; }

        bool is_array() const { return index() == Type::array; }

        bool is_object() const { return index() == Type::object; }

        bool is_null() const { return index() == Type::null; }

        bool is_string() const { return index() == Type::string; }

        bool is_value() const { return index() <= Type::boolean; }

        bool is_value_array() const { return index() >= Type::array_uint64; }

        template<typename T>
        bool checkType() {
            auto pval = std::get_if<T>(value);
            return (pval != nullptr);
        }

        size_type size() const noexcept {
            if (is_null()) return 0;
            else if (is_array()) return get_v<ArrayPtr>(value)->size();
            else if (is_object()) return get_v<ObjPtr>(value)->size();
            else return 1;
        }

        bool operator==(const variant &other) const {
            //FIXME
          //  return value == other.value;
          return true;
        }

        bool operator==(const char *rhs) const {
            if (is_string()) {
                return (*get_v<StrPtr>(value) == rhs);
            }
            return false;
        }

        template<typename T>
        bool operator==(T const &rhs) const {
            bool result = false;
            using DT=typename std::decay<T>::type;
            if constexpr (std::is_same<DT, bool>::value) {
                if (is_bool()) {
                    bool lhs = get_v<bool>(value);
                    result = (lhs == rhs);
                }
            } else if constexpr (std::is_integral<DT>::value) {
                if (is_integer()) {
                    if (is_signed()) {
                        I64 lhs = getInt();
                        result = (lhs == rhs);
                    } else {
                        U64 lhs = getUint();
                        result = (lhs == rhs);
                    }
                }
            } else if constexpr (std::is_floating_point<DT>::value) {
                if (is_float()) {
                    DBL lhs = getFloat();
                    result = (lhs == rhs);
                }
            }
            return result;
        }

        template<typename T>
        bool operator!=(T const &rhs) const {
            bool result = false;
            using DT=typename std::decay<T>::type;
            if constexpr (std::is_same<DT, bool>::value) {
                if (is_bool()) {
                    bool lhs = get_v<bool>(value);
                    result = (lhs != rhs);
                }
            } else if constexpr (std::is_integral<DT>::value) {
                if (is_integer()) {
                    if (is_signed()) {
                        I64 lhs = getInt();
                        result = (lhs != rhs);
                    } else {
                        U64 lhs = getUint();
                        result = (lhs != rhs);
                    }
                }
            } else if constexpr (std::is_floating_point<DT>::value) {
                if (is_float()) {
                    DBL lhs = getFloat();
                    result = (lhs != rhs);
                }
            }
            return result;
        }

        size_type byte_count() const noexcept {
            size_t count = sizeof(*this);
            count += visit_v([](auto &&arg) -> size_type {
                using T = std::decay_t<decltype(arg)>;
                if constexpr (is_specialization<T, ValPtr>::value) {
                    size_type s = arg->size();
                    return s > 0 ? s * sizeof(arg->at(0)) : s;
                } else if constexpr  (std::is_same_v<T, ArrayPtr>) {
                    size_type s = 0;
                    for (auto const &a: *arg) s += a.byte_count();
                    return s;
                } else if constexpr (std::is_same_v<T, ObjPtr>) {
                    size_type s = 0;
                    for (auto const &o: *arg) s += (o.second.byte_count() + o.first.size());
                    return s;
                } else if constexpr (std::is_same_v<T, StrPtr>) return arg->size();
                else return 0;
            }, value);
            return count;
        }


        void dump(unsigned indent=0) const {
            rapidjson::StringBuffer buffer;
            rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
            dump(writer);
            std::cout << buffer.GetString() << std::endl;
        }

        void toJson(rapidjson::StringBuffer &buffer) {
            rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
            dump(writer);
        }

        void fromJson(const char *json) {
            JsonHandler handler(*this);
            rapidjson::Reader reader;
            rapidjson::StringStream ss(json);
            reader.Parse(ss, handler);
        }
      static std::vector<uint8_t> to_msgpack(const variant &v);
      static variant from_msgpack(const std::vector<uint8_t> &data);
      
        typedef struct _parse_error : public std::exception {
            const char *what() const throw() {
                return "Can't parse JSON file";
            }
        } parse_error;


        static variant parse(std::ifstream &file) {
            variant v;
            JsonHandler handler(v);
            rapidjson::Reader reader;
            rapidjson::IStreamWrapper isw(file);
            try {
                reader.Parse(isw, handler);
            } catch (...) {
                throw parse_error();
            }


            return v;
        }

        void dump(rapidjson::Writer<rapidjson::StringBuffer> &w) const noexcept {
            visit_v([&w](auto &&arg) {
                using T = std::decay_t<decltype(arg)>;
                if constexpr (is_specialization<T, ValPtr>::value) {
                    w.StartArray();
                    for (auto e: *arg) {
                        variant v = e;
                        v.dump(w);
                    }
                    //using W=typename U::value_type;
                    w.EndArray();
                } else if constexpr  (std::is_same_v<T, ArrayPtr>) {
                    w.StartArray();
                    for (auto const &a: *arg) a.dump(w);
                    w.EndArray();
                } else if constexpr (std::is_same_v<T, ObjPtr>) {
                    w.StartObject();
                    for (auto const &o: *arg) {
                        w.Key(o.first.c_str());
                        o.second.dump(w);
                    }
                    w.EndObject();
                } else if constexpr (std::is_same_v<T, StrPtr>) w.String(arg->c_str());
                else if constexpr (std::is_same_v<T, std::nullptr_t>) w.Null();
                else if constexpr (std::is_same_v<T, bool>) w.Bool(arg);
                else if constexpr (std::is_floating_point_v<T>) w.Double(DBL(arg));
                else if constexpr (std::is_signed_v<T>) w.Int(arg);
                else if constexpr (std::is_unsigned_v<T>) w.Uint(arg);
                //else std::cout << " " << arg << "\n";
            }, value);
        }

// conversion operators


        template<typename T,
                typename = typename std::enable_if<
                        (!constructs_string<T>::value && std::is_arithmetic_v<T>) || std::is_enum_v<T>
                >::type
        >
        operator T() const {
            if constexpr(std::is_enum_v<T>)
	      return T(getUint());
            else if constexpr(std::is_floating_point<T>::value )
              return T(getFloat());	    
            else if constexpr(std::numeric_limits<T>::is_integer) {
		if(is_signed()) return T(getInt());	       
		else return T(getUint());
	    } 
	    else return get_v<T>(value);
        }

        operator std::string() const {
            return *get_v<StrPtr>(value);

        }

        template<typename T>
        operator std::vector<T>() const {
            return *get_v<ValPtr<T>>(value);
        }


        template<typename String,
                typename=typename std::enable_if<
                        std::is_convertible<String, std::string>::value
                >::type>
        reference operator[](const String &key) {
            if (is_null()) {
                value = ObjPtr();
                get_v<ObjPtr>(value)->operator[](key);
            }
            return get_v<ObjPtr>(value)->operator[](key);
        }

        template<typename String,
                typename=typename std::enable_if<
                        std::is_convertible<String, std::string>::value
                >::type>

        const_reference operator[](const String &key) const {
            if (is_object()) {
                return get_v<ObjPtr>(value)->operator[](key);
            } else throw;
        }

        reference operator[](typename array_t::size_type idx) {
            if (is_null()) {
                value = ArrayPtr();
            }
	    if (is_array()) {
	      ArrayPtr &p=get_v<ArrayPtr>(value);
	      for (size_t i = p->size(); i <= idx; ++i)
                p->push_back(variant());	
	      return p->operator[](idx);
	    }
	    else throw;
	}

        const_reference operator[](size_type idx) const {
            if (is_array()) return get_v<ArrayPtr>(value)->at(idx);
            else throw;
        }


        template<typename T>
        void push_back(const T &val) {
            if (is_null()) {
                value = ArrayPtr();
            }
            get_v<ArrayPtr>(value)->push_back(variant(val));
        }
        template<typename T>
        void emplace_back(const T &val) {
            if (is_null()) {
                value = ArrayPtr();
            }
            get_v<ArrayPtr>(value)->emplace_back(variant(val));
        }

        Type index() const {  
	  return (Type) value.index();
	}

        bool empty() const {
            if (is_null()) return true;
            if (is_array()) return get_v<ArrayPtr>(value)->empty();
            else if (is_object()) return get_v<ObjPtr>(value)->empty();
            return false;
        }


        const std::string type_name() const {
            static const std::vector<std::string> typenames = {
                    "null",
                    "uint64",
                    "uint32",
                    "uint16",
                    "uint8",
                    "int64",
                    "int32",
                    "int16",
                    "int8",
                    "float",
                    "double",
                    "bool",
                    "array",
                    "object",
                    "string",
                    "array_uint64",
                    "array_int64",
                    "array_uint32",
                    "array_int32",
                    "array_uint16",
                    "array_int16",
                    "array_uint8",
                    "array_int8",
                    "array_float",
                    "array_double",
                    "array_bool"
            };
            return typenames[value.index()];
        }

        class JsonHandler : public rapidjson::BaseReaderHandler<rapidjson::UTF8<>, JsonHandler> {
        public:
            JsonHandler(variant &var) : result(var) {}
	  template <typename T> 
            inline void val_handler(const T &value) {
                if (!object.empty()) {
                    variant &top = object.back();
                    if (top.is_array()) {
		      top.emplace_back(value);
                    } else if (top.is_object() && !keys.empty()) {
                        top[keys.top()] = value;
                        keys.pop();
                    }
		  
		}
	    }
	    inline void val_handler(const char *str) {
                if (!object.empty()) {
                    variant &top = object.back();
                    if (top.is_array()) {
		      top.emplace_back(value);
                    } else if (top.is_object() && !keys.empty()) {
                        top[keys.top()] = value;
                        keys.pop();
                    }
		  
		}
	    }
	  inline void val_handler(const variant &value,std::size_t offset) {
                if (object.size()>offset) {
		  variant &top = object[object.size()-offset-1];
                    if (top.is_array()) {
		      top.emplace_back(value);
                    } else if (top.is_object() && !keys.empty()) {
                        top[keys.top()] = value;
                        keys.pop();
                    }
		  
		}
	    }

	    std::vector<variant> object;
            std::stack<std::string> keys;
            variant &result;
        public:
	     bool Null() {
	        val_handler(variant(variant::Type::null));
                return true;
            }

            bool Bool(bool b) {
	      val_handler<bool>(b);
                return true;
            }

            bool Int(int i) {
	      val_handler<int32_t>(i);
                return true;
            }

            bool Uint(unsigned u) {
	      val_handler<uint32_t>(u);
                return true;
            }

            bool Int64(I64 i) {
	        val_handler<I64>(I64(i));
                return true;
            }

            bool Uint64(U64 u) {
	        val_handler<U64>(U64(u));
                return true;
            }

            bool Double(double d) {
	        val_handler<DBL>(DBL(d));
                return true;
            }

            bool RawNumber(const char *str, rapidjson::SizeType length, bool copy) {
                return true;
            }

            bool String(const char *str, rapidjson::SizeType length, bool copy) {
	      val_handler<const char*>(str);
                return true;
            }

            bool StartObject() {
                object.push_back(variant(variant::Type::object));
                return true;
            }

            bool Key(const char *str, rapidjson::SizeType length, bool copy) {
                keys.push(str);
                return true;
            }

            bool EndObject(rapidjson::SizeType memberCount) {
                variant &a = object.back();
                val_handler(a,1);				
                if (object.size()==1) result = a;
		object.pop_back();
                return true;
            }

            bool StartArray() {
                object.push_back(variant(variant::Type::array));
                return true;
            }

            bool EndArray(rapidjson::SizeType elementCount) {
                variant &a = object.back();
                val_handler(a,1);
		object.pop_back();
                return true;
            }
        };

        value_t value;

        friend std::ostream &operator<<(std::ostream &out, const variant<> &var);

        class primitive_iterator_t {
        public:
            using difference_type = std::ptrdiff_t;

            constexpr difference_type get_value() const noexcept {
                return m_it;
            }

            void set_begin() noexcept {
                m_it = begin_value;
            }

            void set_end() noexcept {
                m_it = end_value;
            }

            constexpr bool is_begin() const noexcept {
                return m_it == begin_value;
            }

            constexpr bool is_end() const noexcept {
                return m_it == end_value;
            }

            friend constexpr bool operator==(primitive_iterator_t lhs, primitive_iterator_t rhs) noexcept {
                return lhs.m_it == rhs.m_it;
            }

            friend constexpr bool operator<(primitive_iterator_t lhs, primitive_iterator_t rhs) noexcept {
                return lhs.m_it < rhs.m_it;
            }

            primitive_iterator_t operator+(difference_type i) {
                auto result = *this;
                result += i;
                return result;
            }

            friend constexpr difference_type operator-(primitive_iterator_t lhs, primitive_iterator_t rhs) noexcept {
                return lhs.m_it - rhs.m_it;
            }

            friend std::ostream &operator<<(std::ostream &os, primitive_iterator_t it) {
                return os << it.m_it;
            }

            primitive_iterator_t &operator++() {
                ++m_it;
                return *this;
            }

            primitive_iterator_t operator++(int) {
                auto result = *this;
                m_it++;
                return result;
            }

            primitive_iterator_t &operator--() {
                --m_it;
                return *this;
            }

            primitive_iterator_t operator--(int) {
                auto result = *this;
                m_it--;
                return result;
            }

            primitive_iterator_t &operator+=(difference_type n) {
                m_it += n;
                return *this;
            }

            primitive_iterator_t &operator-=(difference_type n) {
                m_it -= n;
                return *this;
            }

        private:
            static constexpr difference_type begin_value = 0;
            static constexpr difference_type end_value = begin_value + 1;

            difference_type m_it = (std::numeric_limits<std::ptrdiff_t>::min)();
        };

        using internal_iterator=variant_t<
                typename object_t::iterator,
                typename array_t::iterator,
                primitive_iterator_t>;

        class iterator {
            friend variant;
            using value_type = variant;
            using pointer = const value_type *;
            using reference = const value_type &;
            using difference_type = std::ptrdiff_t;
            using iterator_category = std::bidirectional_iterator_tag;
            using array_it=typename array_t::iterator;
            using object_it=typename object_t::iterator;
            using internal_iterator=variant_t<
                    object_it,
                    array_it,
                    primitive_iterator_t>;


            iterator() = default;

        public:
            explicit iterator(pointer object) noexcept : m_object(object) {
                if (m_object->is_object()) m_it = object_it();
                else if (m_object->is_array()) m_it = array_it();
                else m_it = primitive_iterator_t();
            }

            iterator(const iterator &other) noexcept : m_object(other.m_object), m_it(other.m_it) {}

            iterator &operator=(const iterator &other) noexcept {
                m_object = other.m_object;
                m_it = other.m_it;
                return *this;
            }

            reference operator*() const {
                if (m_object->is_array()) return *get_v<array_it>(m_it);
                else if (m_object->is_object()) return get_v<object_it>(m_it)->second;
                else if (get_v<primitive_iterator_t>(m_it).is_begin()) return *m_object;
                else throw;
            }

            pointer operator->() const {
                if (m_object->is_array()) return &*get_v<array_it>(m_it);
                else if (m_object->is_object()) return &get_v<object_it>(m_it)->second;
                else if (get_v<primitive_iterator_t>(m_it).is_begin()) return m_object;
                else throw;
            }

            iterator operator++(int) {
                auto result = *this;
                ++(*this);
                return result;
            }

            iterator &operator++() {
                if (m_object->is_array()) std::advance(get_v<array_it>(m_it), 1);
                else if (m_object->is_object()) std::advance(get_v<object_it>(m_it), 1);
                else
                    get_v<primitive_iterator_t>(m_it)++;
                return *this;
            }

            iterator operator--(int) {
                auto result = *this;
                --(*this);
                return result;
            }

            iterator &operator--() {
                if (m_object->is_array()) std::advance(get_v<array_it>(m_it), -1);
                else if (m_object->is_object()) std::advance(get_v<object_it>(m_it), -1);
                else
                    get_v<primitive_iterator_t>(m_it)--;
                return *this;
            }

            bool operator==(const iterator &other) const {
                if (m_object != other.m_object) throw;
                if (m_object->is_array()) return (get_v<array_it>(m_it) == get_v<array_it>(other.m_it));
                else if (m_object->is_object()) return (get_v<object_it>(m_it) == get_v<object_it>(other.m_it));
                else return (get_v<primitive_iterator_t>(m_it) == get_v<primitive_iterator_t>(other.m_it));

            }

            bool operator!=(const iterator &other) const {
                return not operator==(other);
            }

            bool operator<(const iterator &other) const {
                if (m_object->is_object()) throw;
                else if (m_object->is_array()) return (get_v<array_it>(m_it) < get_v<array_it>(other.m_it));
                else (get_v<primitive_iterator_t>(m_it) < get_v<primitive_iterator_t>(other.m_it));
            }

            bool operator<=(const iterator &other) const {
                return not other.operator<(*this);
            }

            bool operator>(const iterator &other) const {
                return not operator<=(other);
            }

            bool operator>=(const iterator &other) const {
                return not operator<(other);
            }

            iterator &operator+=(difference_type i) {
                if (m_object->is_object()) throw;
                else if (m_object->is_array()) std::advance(get_v<array_it>(m_it), i);
                else (get_v<primitive_iterator_t>(m_it)) += i;
                return *this;
            }

            iterator &operator-=(difference_type i) {
                return operator+=(-i);
            }

            iterator operator+(difference_type i) const {
                auto result = *this;
                result += i;
                return result;
            }

            friend iterator operator+(difference_type i, const iterator &it) {
                auto result = it;
                result += i;
                return result;
            }

            iterator operator-(difference_type i) const {
                auto result = *this;
                result -= i;
                return result;
            }

            difference_type operator-(const iterator &other) const {
                if (m_object->is_object()) throw;
                else if (m_object->is_array()) return get_v<array_it>(m_it) - get_v<array_it>(other.m_it);
                else
                    get_v<primitive_iterator_t>(m_it) - get_v<primitive_iterator_t>(other.m_it);
            }

            reference operator[](difference_type n) const {
                if (m_object->is_object()) throw;
                else if (m_object->is_array()) return *std::next(get_v<array_it>(m_it), n);
                else if (get_v<primitive_iterator_t>(m_it).get_value() == -n) return *m_object;
                else throw;
            }

            typename object_t::key_type key() const {
                if (m_object->is_object()) {
                    return get_v<object_it>(m_it)->first;
                }
                throw;
            }

            reference value() const {
                return operator*();
            }

            using const_iterator=iterator;


        private:

            void set_begin() noexcept {
                if (m_object->is_array()) get_v<array_it>(m_it) = get_v<ArrayPtr>(m_object->value)->begin();
                else if (m_object->is_object()) get_v<object_it>(m_it) = get_v<ObjPtr>(m_object->value)->begin();
                else if (m_object->is_null()) get_v<primitive_iterator_t>(m_it).set_end();
                else
                    get_v<primitive_iterator_t>(m_it).set_begin();
            }

            void set_end() noexcept {
                if (m_object->is_array()) get_v<array_it>(m_it) = get_v<ArrayPtr>(m_object->value)->end();
                else if (m_object->is_object()) get_v<object_it>(m_it) = get_v<ObjPtr>(m_object->value)->end();
                else if (m_object->is_null()) get_v<primitive_iterator_t>(m_it).set_end();
                else
                    get_v<primitive_iterator_t>(m_it).set_end();
            }

            pointer m_object = nullptr;
            internal_iterator m_it;

        };

    public:
        iterator begin() const noexcept {
            iterator result(this);
            result.set_begin();
            return result;
        }

        iterator end() const noexcept {
            iterator result(this);
            result.set_end();
            return result;
        }

        using const_iterator=const iterator;

        template<typename String,
                typename=typename std::enable_if<
                        std::is_convertible<String, std::string>::value
                >::type>
        const_iterator find(const String &key) const {
            auto result = end();

            if (is_object()) {

                result.m_it = get_v<ObjPtr>(value)->find(key);
            }

            return result;
        }

    };
};

#include <MsgPack.hpp>
namespace variant_impl {
  template <unsigned N>
  std::vector<uint8_t> variant<N>::to_msgpack(const variant<N> &v) {
    std::vector<uint8_t> result;
    MemoryWriteBuffer writeBuffer(result);
    msgpack::Writer<N> msgwriter(writeBuffer,v);
    //    std::cout << result.size() <<std::endl;
    return result;
  }
  template <unsigned N>
  variant<N> variant<N>::from_msgpack(const std::vector<uint8_t> &data) {
    MemoryReadBuffer buffer((char*)data.data(),data.size());
    variant<N> v;
    msgpack::Reader<N>(buffer,v);
    return v;
  }
};


using variant=variant_impl::variant<>;
using variant64=variant;
using variant32=  variant_impl::variant<32>;

template<typename T,
        typename std::enable_if<
                (std::is_same<T, variant32>{} || std::is_same<T, variant64>{}),
                int>::type = 0>
std::ostream &operator<<(std::ostream &out, T const &var) {
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    var.dump(writer);
    out << buffer.GetString();
    return out;
}

#endif
